package com.imba.case06_16110100425.listener;


import com.imba.case06_16110100425.bean.WeatherInfo;

/**
 * Created by Junsheng on 2016/12/14.
 */

public interface WeatherListener {
    public void onResponse(WeatherInfo weather);
    public void onFail(String msg);
}
