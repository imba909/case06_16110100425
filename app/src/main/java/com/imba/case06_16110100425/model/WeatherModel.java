package com.imba.case06_16110100425.model;


import com.imba.case06_16110100425.bean.WeatherResult;
import com.imba.case06_16110100425.iface.WeatherIface;
import com.imba.case06_16110100425.listener.WeatherListener;
import com.imba.case06_16110100425.service.WeatherService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Junsheng on 2016/12/14.
 */

public class WeatherModel extends BaseModel implements WeatherIface {
    private WeatherService service;
    private static WeatherModel model;
    private static final String BASE_URL = "http://www.weather.com.cn/";
    public WeatherModel(String url) {
        super(url);
    }
    public WeatherModel(){
        super(BASE_URL);
    }
    public static WeatherModel getInstance(){
        if(model==null){
            model = new WeatherModel();

        }
        return model;
    }

    @Override
    public void getWeather(String cityNumber, final WeatherListener listener) {
        service = createService(WeatherService.class);
        Call<WeatherResult> call = service.getResult(cityNumber);
        call.enqueue(new Callback<WeatherResult>() {
            @Override
            public void onResponse(Call<WeatherResult> call, Response<WeatherResult> response) {
                if(response.isSuccessful()&&response.body()!=null){
                    listener.onResponse(response.body().getWeatherinfo());
                }
                else{
                    listener.onFail("解析错误！");
                }
            }

            @Override
            public void onFailure(Call<WeatherResult> call, Throwable t) {
                listener.onFail(t.toString());
            }
        });
    }
}
