package com.imba.case06_16110100425.model;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Junsheng on 2016/12/14.
 */

public class BaseModel {
    private static Retrofit retrofit;
    public BaseModel(String url){
        retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
    }
    public static <T> T createService(Class<T> serviceClass){
        return retrofit.create(serviceClass);
    }


}
