package com.imba.case06_16110100425.model;


import com.imba.case06_16110100425.bean.WeatherResult;
import com.imba.case06_16110100425.iface.WeatherIface;
import com.imba.case06_16110100425.listener.WeatherListener;
import com.imba.case06_16110100425.service.Weather51Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Junsheng on 2016/12/14.
 */

public class Weather51Model extends BaseModel implements WeatherIface {
    private Weather51Service service;
    private static Weather51Model model;
    private static final String BASE_URL = "http://weather.51wnl.com/weatherinfo/";
    public Weather51Model(String url) {
        super(url);
    }
    public Weather51Model(){
        super(BASE_URL);
    }
    public static Weather51Model getInstance(){
        if(model==null){
            model = new Weather51Model();

        }
        return model;
    }

    @Override
    public void getWeather(String cityNumber, final WeatherListener listener) {
        service = createService(Weather51Service.class);
        Call<WeatherResult> call = service.getResult(cityNumber,1);
        call.enqueue(new Callback<WeatherResult>() {
            @Override
            public void onResponse(Call<WeatherResult> call, Response<WeatherResult> response) {
                if(response.isSuccessful()&&response.body()!=null){
                    listener.onResponse(response.body().getWeatherinfo());
                }
                else{
                    listener.onFail("解析错误！");
                }
            }

            @Override
            public void onFailure(Call<WeatherResult> call, Throwable t) {
                listener.onFail(t.toString());
            }
        });
    }
}
