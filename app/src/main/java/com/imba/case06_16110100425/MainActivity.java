package com.imba.case06_16110100425;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.imba.case06_16110100425.bean.WeatherInfo;
import com.imba.case06_16110100425.factory.WeatherFactory;
import com.imba.case06_16110100425.listener.WeatherListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,WeatherListener {

    private EditText cityNOInput;
    private TextView city;
    private TextView cityNO;
    private TextView temp;
    private TextView wd;
    private TextView ws;
    private TextView sd;
    private ProgressDialog loadingWeather;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cityNOInput =findViewById(R.id.et_city_no);
        city = findViewById(R.id.tv_city);
        cityNO = findViewById(R.id.tv_city_no);
        temp = findViewById(R.id.tv_temp);
        wd = findViewById(R.id.tv_WD);
        ws = findViewById(R.id.tv_WS);
        sd = findViewById(R.id.tv_SD);

        loadingWeather = new ProgressDialog(this);
        loadingWeather.setTitle("查询中...");
        findViewById(R.id.button1).setOnClickListener(this);
        //findViewById(R.id.button2).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //city.setText("查询中...");
        loadingWeather.show();
        switch(v.getId()){
            case R.id.button1:

                //工厂模式
                WeatherFactory.getInstance("weather").getWeather(cityNOInput.getText().toString(),this);
                break;
            case R.id.button2:


                //工厂模式
                WeatherFactory.getInstance("51wnl").getWeather(cityNOInput.getText().toString(),this);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResponse(WeatherInfo weather) {
        loadingWeather.dismiss();
        if(weather!=null){
            city.setText(weather.getCity());
            cityNO.setText(weather.getCityid());
            temp.setText(weather.getTemp());
            wd.setText(weather.getWD());
            ws.setText(weather.getWS());
            sd.setText(weather.getSD());
        }
        else{
            city.setText("未知");
        }

    }

    @Override
    public void onFail(String msg) {
        loadingWeather.dismiss();
        city.setText(msg);
        //Toast.makeText(this,"fail:"+msg,Toast.LENGTH_SHORT).show();
    }
}
