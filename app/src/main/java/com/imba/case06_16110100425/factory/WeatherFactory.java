package com.imba.case06_16110100425.factory;

import com.imba.case06_16110100425.iface.WeatherIface;
import com.imba.case06_16110100425.model.Weather51Model;
import com.imba.case06_16110100425.model.WeatherModel;

/**
 * Created by Junsheng on 2016/12/14.
 */

public class WeatherFactory {
    public static WeatherIface getInstance(String url){
        if(url.equals("weather")){
            //return WeatherModel.getInstance();
            return new WeatherModel();
        }
        else{
            //return Weather51Model.getInstance();
           return new Weather51Model();
        }
    }
}
