package com.imba.case06_16110100425.service;

import com.imba.case06_16110100425.bean.WeatherResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Junsheng on 2016/12/14.
 */

public interface WeatherService {
    @GET("data/sk/{cityNumber}.html")
    Call<WeatherResult> getResult(@Path("cityNumber") String cityNumber);
}
