package com.imba.case06_16110100425.bean;

/**
 * Created by neusoft on 2017/2/11.
 */

public class WeatherResult {
    private WeatherInfo weatherinfo;

    public WeatherInfo getWeatherinfo() {
        return weatherinfo;
    }

    public void setWeatherinfo(WeatherInfo weatherinfo) {
        this.weatherinfo = weatherinfo;
    }
}
