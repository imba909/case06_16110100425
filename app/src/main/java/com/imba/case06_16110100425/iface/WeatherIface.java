package com.imba.case06_16110100425.iface;


import com.imba.case06_16110100425.listener.WeatherListener;

/**
 * Created by Junsheng on 2016/12/14.
 */

public interface WeatherIface {
    void getWeather(String cityNumber, WeatherListener listener);
}
